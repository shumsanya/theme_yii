<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 17.01.2018
 * Time: 12:55
 */

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset1 extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'assets/web/assets/mobirise-icons/mobirise-icons.css',
        'assets/tether/tether.min.css',
        'assets/bootstrap/css/bootstrap.min.css',
        /*'assets/bootstrap/css/bootstrap-grid.min.css',*/
        'assets/bootstrap/css/bootstrap-reboot.min.css',
        'assets/dropdown/css/style.css',
        'assets/animatecss/animate.min.css',
        'assets/socicon/css/styles.css',
        'assets/theme/css/style.css',
        'assets/gallery/style.css',
        'assets/mobirise/css/mbr-additional.css',
    ];
    public $js = [
        'assets/web/assets/jquery/jquery.min.js',
        'assets/popper/popper.min.js',
        'assets/tether/tether.min.js',
        'assets/bootstrap/js/bootstrap.min.js',
        'assets/smoothscroll/smooth-scroll.js',
        'assets/dropdown/js/script.min.js',
        'assets/touchswipe/jquery.touch-swipe.min.js',
        'assets/parallax/jarallax.min.js',
        'assets/masonry/masonry.pkgd.min.js',
        'assets/imagesloaded/imagesloaded.pkgd.min.js',
        'assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js',
        'assets/vimeoplayer/jquery.mb.vimeo_player.js',
        'assets/viewportchecker/jquery.viewportchecker.js',
        'assets/theme/js/script.js',
        'assets/gallery/player.min.js',
        'assets/gallery/script.js',
        'assets/slidervideo/script.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}