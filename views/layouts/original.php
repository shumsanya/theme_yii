<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset1;

AppAsset1::register($this);
?>
<?php $this->beginPage() ?>

    <!DOCTYPE html>
    <html>
    <head>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
<!--Section Меню -->
    <section class="menu cid-qGYHY8TDk3" once="menu" id="menu2-m">
        <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-toggleable-sm" style="padding:0px;height:59px;min-height: 59px;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class="menu-logo">
                <div class="navbar-brand" style="margin-left: 32px">
                <span class="navbar-logo">
                    <a href="#">
                        <img src="assets/images/logo2.svg" alt="Travel" style="height: 90px ;margin-top: 29px">
                    </a>
                </span>

                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="padding-right: 10px">
                <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true" >
                    <li class="nav-item dropdown open">
                        <a class="nav-link link text-primary dropdown-toggle display-4" href="#" target="_blank"
                           data-toggle="dropdown-submenu" aria-expanded="true">&nbsp;Руский &nbsp;</a>
                        <div class="dropdown-menu">
                            <a class="text-primary dropdown-item display-4" href="#" target="_blank">1 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#" target="_blank"
                               aria-expanded="true">2 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#" target="_blank"
                               aria-expanded="false">3 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#" target="_blank"
                               aria-expanded="false">4 New Item</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link text-primary dropdown-toggle display-4" href="https://mobirise.com"
                           data-toggle="dropdown-submenu" aria-expanded="false">&nbsp;EUR( ) &nbsp;</a>
                        <div class="dropdown-menu">
                            <a class="text-primary dropdown-item display-4" href="#">
                                1 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#"
                               aria-expanded="true">2 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#"
                               aria-expanded="false">3 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#"
                               aria-expanded="true">4 New Item</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link text-primary display-4" href="#">
                            <span class="mbri-shopping-cart mbr-iconfont mbr-iconfont-btn"
                                  style="font-size: 15px; color: rgb(35, 35, 35);"></span>&nbsp;Корзина
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link text-primary display-4" href="#">
                            <span class="mbri-question mbr-iconfont mbr-iconfont-btn"
                                  style="font-size: 15px; color: rgb(35, 35, 35);"></span>&nbsp;Помощь
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link text-primary dropdown-toggle display-4" href="#"
                           data-toggle="dropdown-submenu" aria-expanded="false">
                            <span class="mbri-user mbr-iconfont mbr-iconfont-btn"
                                  style="font-size: 15px; color: rgb(35, 35, 35);"></span>&nbsp;Войти
                        </a>
                        <div class="dropdown-menu"><a class="text-primary dropdown-item display-4"
                                                      href="#">1 New Item</a>
                            <a class="text-primary dropdown-item display-4" href="#"
                               aria-expanded="true">2 New Item</a>
                        </div>
                    </li>
                </ul>
                <div class="navbar-buttons mbr-section-btn">
                    <a class="btn btn-sm btn-primary display-4" href="#" target="_blank" style="padding: 10px 5px 10px 5px">
                        <span class="mbri-user mbr-iconfont mbr-iconfont-btn"
                              style="font-size: 15px; color: rgb(35, 35, 35);"></span>Зарегистрируйтесь
                    </a>
                </div>
            </div>
        </nav>
    </section>

<!--Section Фото + Заголовок + Форма пошуку -->
    <section class="cid-qGURMEosy1 mbr-fullscreen mbr-parallax-background" id="header2-f" >

        <div class="container ">
            <div class="row ">
                <div class="mbr-white col-md-10" style="position: relative;float: left;width: 100%;min-height: 1px;padding-left: 16px;padding-right: 16px;padding-left: 0;margin-bottom: 6px;">
                    <h1 class="" style="display: inline;
background: #fff;
padding: 0.6rem;
box-decoration-break: clone;
font-size: 6rem;
line-height: 1.3;
font-weight: normal;
color: #4E4E4F;
font-family: 'Lato',Arial,sans-serif;">Заказывайте билеты на лучшие развлечения по всему миру</h1>

                    <div class="">
                        <form class="navbar-form navbar-left search-form" role="search" style="padding-left: 9px;">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Куда вы едете ?" autocomplete="off" style="border: 1px solid #ababab">
                                <label class="" for="date-from-input">
                                    <span class=""> c </span>
                                    <input id="date-from-input" type="date" class="form-control" placeholder="с" style="border: 1px solid #ababab">
                                </label>
                                <span class=""><b> по</b></span>
                                <input type="date" class="form-control" placeholder="по" style="border: 1px solid #ababab">
                            </div>
                            <button type="submit" class="btn btn-default" style="color: whitesmoke; background-color: #007bff"><span class="glyphicon glyphicon-search"></span>&nbsp; Поиск</button>
                        </form>
                    </div>

                </div>
            </div>

        </div>
        <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
            <a href="#next">
                <i class="mbri-down mbr-iconfont" style="top: 5px; left: 20px;"></i>
            </a>
        </div>
    </section>

<!--Section Блок з модальним вікном -->
    <section class="" style=" border: 1px solid #E6E6E6;border-top: 3px solid #00AEEF;background-color: #fff;display: block;cursor: pointer;">
        <div class="container" data-toggle="modal" data-target="#myModal" style="padding-left: 32px; padding-right: 32px; margin-right: auto;margin-left: auto; min-width: 320px;width: 100%;/*box-sizing: border-box;*/ ">
     <!--модальне вікно-->
            <div id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="display: block">
                            <div>
                                <button class="modal-button" type="button" data-dismiss="modal">×</button>
                            </div>
                            <h1 class="modal-title">Почему с нами ?</h1>
                        </div>
                        <div class="modal-body">
                            Самая большая в мире коллекция путешествий и приключений

                            Прямо сейчас вам доступны 31 520 интересных событий. Чего бы вы ни пожелали — насладиться вкуснейшими тапас в Испании или поплавать с маской среди разноцветных кораллов в Квинсленде — здесь будут варианты на любой вкус.
                            Профессиональные местные гиды

                            Сообщество местных поставщиков GetYourGuide знает свое дело. В конце концов наша работа — показать вам мир! Мы строго отбираем наших партнеров, чтобы вы точно были в безопасности и отлично провели время.
                            Гарантия лучшей цены

                            Потрясающие воспоминания о поездках могут быть бесценны, но хорошо еще и знать, что вы заключили выгодную сделку. Если найдет то же самое, но дешевле, дайте нам знать, и мы вернём вам разницу — без вопросов.
                            Отличная служба поддержки

                            Отделения в Северной Америке и Европе на постоянной связи с клиентами. Готовы ли вы к приключениям или пока еще просто шарите в сети, просто позвоните или отправьте сообщение. Мы рады помочь!
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button" data-dismiss="modal" style="background-color: #007bff">Ясно</button>
                        </div>
                    </div>
                </div>
            </div>
     <!--модальне вікно and-->
        <div class="row services-overviem" style="display: table;">

            <div class="col-md-3 hidden-sm col-xs-12 services-overviem-claim getYourGuide">
                <p class="services-claim">GetYourGuide предлагает</p>
            </div>
            <div class="col-md-3 col-xs-12 services-column-item ">
                <p class="services-item services-item--crown icon icon-crown">Лучший выбор</p>
                <p class="services-item-info">Более 31 410 интересных занятий</p>
            </div>
            <div class="col-md-3  col-xs-12 services-column-item">
                <p class="services-item services-item--crown icon icon-crown">Цены</p>
                <p class="services-item-info">Более 31 410 интересных занятий</p>
            </div>
            <div class="col-md-3  col-xs-12 services-column-item">
                <p class="services-item services-item--crown icon icon-crown">Лучший выбор</p>
                <p class="services-item-info">Более 31 410 интересных занятий</p>
            </div>
        </div>
    </div>
    </section>

<!--Section Галерея -->
    <section class="mbr-gallery mbr-slider-carousel cid-qGUS2Mzscq" id="gallery1-i">
        <div>
            <div><!-- Filter --><!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Awesome">
                                    <div href="#lb-gallery1-i" data-slide-to="0" data-toggle="modal">
                                        <img src="assets/images/gallery00.jpg" alt="">
                                        <span class="icon-focus"></span>
                                        <span class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Responsive">
                                    <div href="#lb-gallery1-i" data-slide-to="1" data-toggle="modal">
                                        <img src="assets/images/gallery01.jpg" alt="">
                                        <span class="icon-focus"></span>
                                        <span class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Creative">
                                    <div href="#lb-gallery1-i" data-slide-to="2" data-toggle="modal"><img
                                                src="assets/images/gallery02.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Animated">
                                    <div href="#lb-gallery1-i" data-slide-to="3" data-toggle="modal"><img
                                                src="assets/images/gallery03.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Awesome">
                                    <div href="#lb-gallery1-i" data-slide-to="4" data-toggle="modal"><img
                                                src="assets/images/gallery04.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Awesome">
                                    <div href="#lb-gallery1-i" data-slide-to="5" data-toggle="modal"><img
                                                src="assets/images/gallery05.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Responsive">
                                    <div href="#lb-gallery1-i" data-slide-to="6" data-toggle="modal"><img
                                                src="assets/images/gallery06.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false"
                                     data-tags="Animated">
                                    <div href="#lb-gallery1-i" data-slide-to="7" data-toggle="modal"><img
                                                src="assets/images/gallery07.jpg" alt=""><span
                                                class="icon-focus"></span><span
                                                class="mbr-gallery-title mbr-fonts-style display-7">Type caption here</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div><!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1"
                     data-keyboard="true" data-interval="false" id="lb-gallery1-i">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <ol class="carousel-indicators">
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="0"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="1"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i" class=" active"
                                        data-slide-to="2"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="3"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="4"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="5"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="6"></li>
                                    <li data-app-prevent-settings="" data-target="#lb-gallery1-i"
                                        data-slide-to="7"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item"><img src="assets/images/gallery00.jpg" alt=""></div>
                                    <div class="carousel-item"><img src="assets/images/gallery01.jpg" alt=""></div>
                                    <div class="carousel-item active"><img src="assets/images/gallery02.jpg" alt="">
                                    </div>
                                    <div class="carousel-item"><img src="assets/images/gallery03.jpg" alt=""></div>
                                    <div class="carousel-item"><img src="assets/images/gallery04.jpg" alt=""></div>
                                    <div class="carousel-item"><img src="assets/images/gallery05.jpg" alt=""></div>
                                    <div class="carousel-item"><img src="assets/images/gallery06.jpg" alt=""></div>
                                    <div class="carousel-item"><img src="assets/images/gallery07.jpg" alt=""></div>
                                </div>
                                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev"
                                   href="#lb-gallery1-i"><span class="mbri-left mbr-iconfont"
                                                               aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a><a
                                        class="carousel-control carousel-control-next" role="button" data-slide="next"
                                        href="#lb-gallery1-i"><span class="mbri-right mbr-iconfont"
                                                                    aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a><a class="close" href="#" role="button"
                                                                              data-dismiss="modal"><span
                                            class="sr-only">Close</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

<!--Section Вкладені блоки -->
    <section class="" style="margin-bottom: 20px">
        <div class="align-center">
            <button type="button" class="btn btn-primary" style="font-size: 16px">Показать другие достопримечательности</button>
        </div>

        <div class="container tabs-component">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#">Лутшие направления</a></li>
                <li><a href="#">Лутшие 10 стран</a></li>
            </ul>

            <!--<div class="container tabs-component">
                <header class="nav-tabs-header">
                    <ul class="nav-tabs">
                        <li class="table-active">
                            <a class="tab-link" href="#">Лутшие направления</a>
                        </li>
                        <li class="table">
                            <a class="tab-link" href="#">Лутшие 10 стран</a>
                        </li>
                    </ul>
                </header> -->
                <div class="tab-content">
                    <div id="itinerariesAttractions" class="row tab-pane active">
                        <div class="row col-sm-12 margin-top">
                            <div class="col-sm-12">
                                Хотите узнать, в каких еще странах у нас есть мероприятия?
                                <a href="#">Посмотрите все наши туры и мероприятия по направлениям.</a>
                            </div>
                        </div>
                    </div>
                    <div id="countries" class="row tab-pane active">
                        <div class="top-countries-row">
                            <div class="row col-sm-12">
                                <div class="col-sm-12">
                                    <h2>Лучшие 10 стран</h2>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--Section Футер -->
    <section class="cid-qGURMFhT1p " id="footer1-g">
        <div class="container">
            <div class="">
                <div class="media-container-row content text-white">

                    <div class="col-12 col-md-4">
                        <div class="media-wrap">

                            <label class="footer-label" for="language-selector">Язык</label>
                            <div class="selector">
                                <select class="selector-sel" id="language-selector">
                                    <option selected="">Руский</option>
                                    <option value="#">Dansk</option>
                                    <option value="#">Deutsch</option>
                                </select>
                            </div>

                            <label class="footer-label" for="language-selector">Валюта</label>
                            <div class="selector">
                                <select class="selector-sel" id="language-selector">
                                    <option selected="">Євро</option>
                                    <option value="#">Злотий</option>
                                    <option value="#">Долар</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-md-2 mbr-fonts-style display-7">
                        <strong class="footer-label" style="font-size: 15px">Приложения</strong>
                        <ul class="footer-ui">
                            <li class="">
                                <div class="">
                                    <img src="assets/images/google-play-badge-ru.svg" >
                                </div>
                            </li> <li class="">
                                <div class="">
                                    <img src="assets/images/app-store-badge-ru.svg">
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="col-12 col-md-2 mbr-fonts-style display-7">
                        <strong class="footer-label" style="font-size: 15px">Поддержка</strong>
                        <p class="mbr-text">
                            <a class="text-primary" href="#">Website</a>
                            <br>
                            <a class="text-primary" href="#">Контакты</a>
                            <br>
                            <a class="text-primary" href="#">Информация</a>
                        </p>
                    </div>

                    <div class="col-12 col-md-2 mbr-fonts-style display-7">
                        <strong class="footer-label" style="font-size: 15px">Компания</strong>
                        <p class="mbr-text">
                            <a class="text-primary" href="#">Website</a>
                            <br>
                            <a class="text-primary" href="#">Контакты</a>
                            <br>
                            <a class="text-primary" href="#">Информация</a>
                        </p>
                    </div>

                    <div class="col-12 col-md-3 mbr-fonts-style display-7">
                        <strong class="footer-label" style="font-size: 15px">Работа с нами</strong>
                        <p class="mbr-text">
                            <a class="text-primary" href="#">Website</a>
                            <br>
                            <a class="text-primary" href="#">Контакты</a>
                            <br>
                            <a class="text-primary" href="#">Информация</a>
                        </p>
                    </div>

                </div>
            </div>
            <div class="footer-lower">
                <div class="media-container-row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="media-container-row mbr-white">
                    <div class="col-sm-6 copyright">
                        <p class="mbr-text mbr-fonts-style display-7">
                            © Copyright 2018
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="social-list align-right">
                            <div class="soc-item">
                                <a href="https://twitter.com/mobirise" target="_blank">
                                    <span class="socicon-twitter socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.facebook.com/pages/Mobirise/1616226671953247" target="_blank">
                                    <span class="socicon-facebook socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.youtube.com/c/mobirise" target="_blank">
                                    <span class="socicon-youtube socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://instagram.com/mobirise" target="_blank">
                                    <span class="socicon-instagram socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://plus.google.com/u/0/+Mobirise" target="_blank">
                                    <span class="socicon-googleplus socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                            <div class="soc-item">
                                <a href="https://www.behance.net/Mobirise" target="_blank">
                                    <span class="socicon-behance socicon mbr-iconfont mbr-iconfont-social"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div id="scrollToTop" class="scrollToTop mbr-arrow-up" style="right: 13px/*position: absolute; right: 5px; bottom: -40px*/">
        <a style="text-align: center;"><i></i></a>
    </div>

    <input name="animation" type="hidden">

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>